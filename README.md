# Passcreator.PasswordHistory

A package that provides a custom account service that works as a drop-in replacement for the default one of Flow.
It stores the credentials source that has been used previously in another table and when a new password is set also checks if the password that should be set is part of the history.

## Setup

To install the package, use composer.

```
composer require passcreator/passwordhistory
```

Run migrations to add the password history table afterwards.
```
./flow doctrine:migrate
```

## Usage

Simply inject the AccountService of this package when setting passwords for your users. The setPassword function requires an additional parameter called passwordHistoryLength that - if it's higher than 0 - triggers the service to check if the password has been used before. E.g. when setting the parameter to 3 you need to specify three different passwords until you can re-use a password.
