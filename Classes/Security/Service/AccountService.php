<?php

namespace Passcreator\PasswordHistory\Security\Service;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Aop\JoinPointInterface;
use Neos\Flow\Security\Account;
use Neos\Flow\Security\Cryptography\HashService;
use Passcreator\PasswordHistory\Domain\Model\PastAccountCredential;
use Passcreator\PasswordHistory\Domain\Repository\PastAccountCredentialRepository;
use Passcreator\PasswordHistory\Exception\PasswordInHistoryException;

/**
 * An aspect that checks if a password has been used previously
 *
 */
class AccountService {

    /**
     * @var HashService
     * @Flow\Inject
     */
    protected $hashService;

    /**
     * @var PastAccountCredentialRepository
     * @Flow\Inject
     */
    protected $pastAccountCredentialRepository;

    /**
     * Checks if the given password has been used previously
     *
     * @param Account $account
     * @param string  $password
     * @param int     $passwordHistoryLength the number of times a password can't be used again
     *
     * @return bool
     */
    protected function hasPasswordBeenUsedPreviously(Account $account, string $password, int $passwordHistoryLength) {
        if ($passwordHistoryLength === 0) {
            return false;
        }

        if ($this->hashService->validatePassword($password, $account->getCredentialsSource())) {
            // the old password has been used so don't allow it.
            return true;
        }

        $pastAccountCredentials = $this->pastAccountCredentialRepository->findByAccount($account,
            $passwordHistoryLength);

        foreach ($pastAccountCredentials as $pastAccountCredential) {
            if ($this->hashService->validatePassword($password,
                    $pastAccountCredential->getCredentialsSource()) === true) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Account $account
     * @param string  $password
     * @param int     $passwordHistoryLength the number of times a password can't be used again
     *
     * @return Account
     * @throws PasswordInHistoryException
     * @throws \Neos\Flow\Persistence\Exception\IllegalObjectTypeException
     */
    public function setPassword(Account $account, string $password, int $passwordHistoryLength = 0) {

        if ($this->hasPasswordBeenUsedPreviously($account, $password, $passwordHistoryLength)) {
            throw new PasswordInHistoryException();
        }

        $pastAccountCredential = new PastAccountCredential();
        $pastAccountCredential->setCredentialsSource($account->getCredentialsSource());
        $pastAccountCredential->setCreatedOn(new \DateTime());
        $pastAccountCredential->setAccount($account);
        $this->pastAccountCredentialRepository->add($pastAccountCredential);

        $account->setCredentialsSource($this->hashService->hashPassword($password));

        return $account;
    }

}
