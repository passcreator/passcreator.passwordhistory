<?php

namespace Passcreator\PasswordHistory\Exception;

/**
 * Exception that is thrown if the password a user tries to set has been used before
 *
 * @api
 */
class PasswordInHistoryException extends \Neos\Flow\Exception {

}

?>
