<?php

namespace Passcreator\PasswordHistory\Domain\Repository;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;
use Neos\Flow\Security\Account;

/**
 * A repository for Past account credential entries
 *
 * @Flow\Scope("singleton")
 */
class PastAccountCredentialRepository extends Repository {

    /**
     * @param Account $account
     * @param int     $limit
     *
     * @return \Neos\Flow\Persistence\QueryResultInterface
     */
    public function findByAccount(Account $account, int $limit) {
        $query = $this->createQuery();
        return $query->matching(
            $query->equals('account', $account)
        )
            ->setOrderings(array(
                'createdOn' => \Neos\Flow\Persistence\QueryInterface::ORDER_DESCENDING
            ))
            ->setLimit($limit)
            ->execute();
    }

}

?>
