<?php

namespace Passcreator\PasswordHistory\Domain\Model;

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use Neos\Flow\Security\Account;
use Neos\Media\Domain\Model\Asset;

/**
 *
 * @Flow\Scope("prototype")
 * @Flow\Entity
 */
class PastAccountCredential {

    /**
     * @var Account
     * @ORM\ManyToOne
     */
    protected $account;

    /**
     * @var \DateTime
     */
    protected $createdOn;

    /**
     * @var string
     */
    protected $credentialsSource;

    /**
     * @return Account
     */
    public function getAccount() {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account) {
        $this->account = $account;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn() {
        return $this->createdOn;
    }

    /**
     * @param \DateTime $createdOn
     */
    public function setCreatedOn($createdOn) {
        $this->createdOn = $createdOn;
    }

    /**
     * @return string
     */
    public function getCredentialsSource() {
        return $this->credentialsSource;
    }

    /**
     * @param string $credentialsSource
     */
    public function setCredentialsSource($credentialsSource) {
        $this->credentialsSource = $credentialsSource;
    }

}
